import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph(2000,50000);
//      Vertex v1 = new Vertex("V1", null, null);
//      Vertex v2 = new Vertex("V2", null, null);
//      Vertex v3 = new Vertex("V3", null, null);
//      Vertex v4 = new Vertex("V4", null, null);
//      g.first = v1;
//      v1.next = v2;
//      v2.next = v3;
//      v3.next = v4;
//
//      Arc v1v2 = new Arc("v1v2", v2, null, 5, v1);
//      Arc v1v4 = new Arc("v1v4", v4, null, 5, v1);
//      Arc v1v3 = new Arc("v1v3", v3, null, 1, v1);
//      Arc v2v1 = new Arc("v2v1", v1, null, 5, v2);
//      Arc v2v3 = new Arc("v2v3", v3, null, 2, v2);
//      //Arc v2v4 = new Arc("v2v4", v4, null, 1, v2);
//      Arc v3v2 = new Arc("v3v2", v2, null, 2, v3);
//      Arc v3v1 = new Arc("v3v1", v1, null, 1, v3);
//      Arc v3v4 = new Arc("v3v4", v4, null, 1, v3);
//      Arc v4v3 = new Arc("v4v3", v3, null, 1, v4);
//      Arc v4v1 = new Arc("v4v1", v1, null, 5, v4);
//     // Arc v4v2 = new Arc("v4v2", v2, null, 1, v4);
//
//      v1.first = v1v2;
//      v1v2.next = v1v4;
//      v1v4.next = v1v3;
//      v2.first = v2v1;
//      v2v1.next = v2v3;
//      //v2v3.next = v2v4;
//      v3.first = v3v2;
//      v3v2.next = v3v4;
//      v3v4.next = v3v1;
//      v4.first = v4v3;
//      v4v3.next = v4v1;
//      //v4v1.next = v4v2;





      System.out.println (g);
      long startTime = System.nanoTime();
      g.minSpanningTree();
      long endTime   = System.nanoTime();
      long totalTime = endTime - startTime;
      System.out.println (g);
      System.out.println(totalTime);

   }

    /** Vertex Class.
     *  insertSortedArcs - inserts and sorts arcs to arcList from new vertex
     *  lenghtCompare - comparator to compare arc's weight
     *  growTree - methode to add new vertex to spanning tree
     */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
        /**
         * Adds arcs of a vertex arcArray arrayList
         * @param arcArray all vertexes that weren't used/checked yet
         * @param incommingArc arc that lead to this vertex being added to MSP
         */
       public List<Arc> insertSortedArcs(List<Arc> arcArray, Arc incommingArc){
            Arc arc = this.first;
            //Arc firstArc = this.first;
            arc.start = this;
            arcArray.add(arc);
            while (arc.next != null){
                arc = arc.next;
                arc.start = this;
//                if(arc.start == incommingArc.target && arc.target == incommingArc.start && arc.info == incommingArc.info){
//                    this.first = arc;
//                    this.first.next = null;
//                }
                arc.start = this;
                arcArray.add(arc);
            }
//            if(incommingArc == null){
//                this.first = firstArc;
//                this.first.next = null;
//            }else if(firstArc.start == incommingArc.target && firstArc.target == incommingArc.start && firstArc.info == incommingArc.info){
//                this.first = firstArc;
//                this.first.next = null;
//            }

            Collections.sort(arcArray, new lengthCompare());
            return arcArray;
        }

       class lengthCompare implements Comparator<Arc>
       {
           public int compare(Arc a, Arc b)
           {
               return a.info - b.info;
           }
       }
        /**
         * Grow MSP one vertex at a time
         * @param linkedVetex array list of all so far linked vertexes
         * @param arcArray all unchecked arcs that belong to vertexses of already grown MSP
         *  @param incommingArc arc that lead to this vertex being added to MSP
         */
       public void growTree(List<Vertex> linkedVetex, List<Arc> arcArray, Arc incommingArc){
           arcArray = this.insertSortedArcs(arcArray, incommingArc);
           this.next = null;
           linkedVetex.add(this);
           if(incommingArc.start == null){
               this.first = null;
           } else {
               for (int i = 0; i < arcArray.size(); i++) {
                   if (arcArray.get(i).start == incommingArc.target && arcArray.get(i).target == incommingArc.start && arcArray.get(i).info == incommingArc.info) {
                       this.first = arcArray.get(i);
                       this.first.next = null;
                   }
               }
           }
           while (arcArray.size()>0){
               Arc currentArc = arcArray.get(0);
               //currentArc.next = null;
               if (linkedVetex.contains(currentArc.target)){arcArray.remove(0);}
               else {
                   incommingArc = currentArc;
                   this.next = currentArc.target;
                   currentArc.next=null;
                   if(currentArc.start.first != null){
                       currentArc.start.first.addArc(currentArc);
                   }
                   if(currentArc.start.first == null){
                       currentArc.start.first = currentArc;
                       currentArc.start.first.next = null;
                   }
               }
               //arcArray.remove(0);
               if(!linkedVetex.contains(currentArc.target)){currentArc.target.growTree(linkedVetex, arcArray, incommingArc);}
               }
           }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

       private String id;
       private Vertex start;
       private Vertex target;
       private Arc next;
       private int info = 0;
       // You can add more fields, if needed

       Arc (String s, Vertex v, Arc a,int length, Vertex start) {
           id = s;
           target = v;
           start = start;
           next = a;
           info = length;
       }

      Arc (String s) {
         this (s, null, null,0, null);
      }

      @Override
      public String toString() {
         return id;
      }

       //Add arc to the end of Vertex arcs
       public void addArc(Arc currentArc){
           if (this != currentArc && this.next == null){
               this.next = currentArc;
               this.next.next = null;
           }else if(this != currentArc){
               this.next.addArc(currentArc);
           }
       }



   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.info);
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
       /**
        * Creates MSP from graph
        */
       public void minSpanningTree(){
           if(this.first == null){return;}
           else{
           List<Vertex> linkedVetex = new ArrayList<Vertex>();
           List<Arc> arcArray =  new ArrayList<Arc>();
           Vertex currentVertex = this.first;
           Arc incommingArc = new Arc(null, null, null,0, null);
           currentVertex.growTree( linkedVetex, arcArray, incommingArc);
           }
       }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
   }

} 

